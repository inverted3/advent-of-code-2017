let input = require('fs').readFileSync('12-input.txt',{encoding: 'utf8'})

input = input.split('\n')
input = input.map((line) => line.split ('<->'))

let map = {}

for (let line of input){
  map[parseInt(line[0])] = line[1].split(',').map((item)=> parseInt(item))
}

let connectedToZero = []

let visitMap = Array(input.length).fill(false)

let addConnected = function(node){
  let currentNode = node
  visitMap[currentNode] = true
  for (let connection of map[node]){
    if(!visitMap[connection]){
      addConnected(connection)
    }
  }
}

let islands = 0 // Disconnected groups
let pointer = 0
while(visitMap.filter(item => !item).length){
  if (!visitMap[pointer]){
    console.log('traversing', pointer)
    addConnected(pointer)
    islands++
  }
  pointer++
}

console.log(islands)