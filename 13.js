let input = require('fs').readFileSync('13-input.txt',{encoding: 'utf8'})

input = input.split('\n').map(line => line.split(':').map(item => parseInt(item)))

let firewall = Array(input[input.length-1][0]+1).fill(0)

for (let line of input){
  firewall[line[0]] = line[1]
}

let getScannerPosition = function(firewall,time, layer){
  let depth = firewall[layer]
  time %= depth * 2 - 2
  if (time < depth) return time
  return depth - (time % depth + 2)
}

let delay = 0
let caught = true

while(caught){
  caught = false
  let position = 0
  while(!caught && position<firewall.length){
    if (firewall[position]>0 && getScannerPosition(firewall,position+delay,position)==0){
      caught = true
      delay++
    }
    position++
  }
  // if (delay%10000==0) console.log('delay was',delay,'caught at layer ',position)
}


console.log(delay)
