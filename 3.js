let input = 347991

let circle = function(number){
  let result = Math.sqrt(number-1)
  result = Math.floor(result)
  if (result%2 ==0) result--
  return result+2
}

let posOnCircle = function(number){
  let result = number - Math.pow(circle(number)-2,2)
  return result-1
}

let numberInMiddle = function(circle, side){
  let circleStartNumber = Math.pow(circle-2,2)+1
  let firstOffset = Math.floor(circle/2) -1
  let offset =  circle-1
  return circleStartNumber + firstOffset + (offset*side)
}

let distFromDirect = function(number){
  let dist = Infinity
  for (let i=0; i<4; i++){
    if (Math.abs(number-numberInMiddle(circle(number),i))<dist){
      dist = Math.abs(number-numberInMiddle(circle(number),i))
    }
  }
  return dist
}

let circleDistance = function(number){
  return Math.floor(circle(number)/2)
}

let totalDistance = function(number){
  return distFromDirect(number)+circleDistance(number)
}


console.log(totalDistance(input)) // Star 1


// 37  36  35  34  33  32  31
// 38  17  16  15  14  13  30
// 39  18   5   4   3  12  29
// 40  19   6   1   2  11  28
// 41  20   7   8   9  10  27
// 42  21  22  23  24  25  26
// 43  44  45  46  47  48  49