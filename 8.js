const fs = require('fs')
const _ = require('lodash')

let inputFileStream = fs.createReadStream('8-input.txt')
let lineReader = require('readline').createInterface(inputFileStream)

let lines = []
let parsedLines = []

let registers = {}

lineReader.on('line', function(line){
  lines.push(line)
  let splitLine = line.split(' ')
  let parsedLine = {
    register: splitLine[0],
    order: splitLine[1]=='inc' ? 1 : -1,
    amount: splitLine[2],
    registerToCheck: splitLine[4],
    comparator: splitLine[5],
    comparisonValue: splitLine[6]
  }
  parsedLines.push(parsedLine)
  registers[parsedLine.register] = 0
})

let compare = function(registerToCheck, comparator, comparisonValue){
  switch (comparator){
    case '<':
      return registerToCheck < comparisonValue
    case '>':
      return registerToCheck > comparisonValue
    case '==':
      return registerToCheck == comparisonValue
    case '>=':
      return registerToCheck >= comparisonValue
    case '<=':
      return registerToCheck <= comparisonValue    
    case '!=':
      return registerToCheck != comparisonValue
  }
}

lineReader.on('close', function(){
  let maxEverHeld = -Infinity
  for (let parsedLine of parsedLines){
    if(compare(registers[parsedLine.registerToCheck], parsedLine.comparator, parsedLine.comparisonValue)){
      registers[parsedLine.register] += (parsedLine.amount*parsedLine.order)
      if (registers[parsedLine.register]>maxEverHeld){
        maxEverHeld = registers[parsedLine.register]
      }
    }
  }
  let max = -Infinity
  for (let register in registers){
    if (registers[register]>max) max = registers[register]
  }
  console.log(max, maxEverHeld)
})