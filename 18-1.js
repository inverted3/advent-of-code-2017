let input = require('fs').readFileSync('18-input.txt',{encoding: 'utf8'}).split('\n')

let registers = {}

const getValue = function(value){
  let result
  if (isNaN(parseInt(value))){
    result = registers[value]
  } else {
    result = parseInt(value)
  }
  return result
}

let pointer = 0

let lastPlayedNote = false

while(pointer>=0 && pointer < input.length){
  console.log('executing line ', pointer, input[pointer].split(' '))
  let command = input[pointer].split(' ')
  let instruction = command[0]
  let firstArgument = command[1]
  if (!registers.hasOwnProperty(firstArgument)){
    registers[firstArgument] = 0
  }
  let secondArgument = getValue(command[2])
  if(isNaN(secondArgument)) {
    console.warn('not a number')
  }
  console.log(command, instruction, firstArgument, secondArgument)
  
  pointer++
  
  switch(instruction){
    case 'snd':
      console.log('Playing sound! ', getValue(firstArgument))
      lastPlayedNote = getValue(firstArgument)
      break
    case 'set':
      registers[firstArgument] = secondArgument
      break
    case 'add':
      registers[firstArgument] += secondArgument
      break
    case 'mul':
      registers[firstArgument] *= secondArgument
      break
    case 'mod':
      registers[firstArgument] %= secondArgument
      break
    case 'rcv':
      if (registers[firstArgument] != 0){
        console.log('Recovering sound! ', lastPlayedNote)
      }
      process.exit()  // Star 1
      break
    case 'jgz':
      if (registers[firstArgument] > 0){
        pointer += secondArgument - 1  // because the pointer was already advanced by one
      }
      break
  }
  console.log(JSON.stringify(registers), pointer)
}

console.log('done')