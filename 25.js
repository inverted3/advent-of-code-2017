let tape = [0]
let cursor = 0
let currentInstruction = 'A'
let moveRight = () => {
  if (cursor+1<tape.length+1) {
    tape.push(0)
  }
  cursor++
}

let moveLeft = () => {
  if (cursor-1<0) {
    tape.unshift(0)
  } else {
    cursor--
  }
}

let iterations = 0

const iterationsToComplete = 12317297

let instructions = {
  'A':{
    0: () => {
      tape[cursor] = 1
      moveRight()
      currentInstruction = 'B'
    },
    1: () => {
      tape[cursor] = 0
      moveLeft()
      currentInstruction = 'D'
    }
  },
  'B':{
    0: () => {
      tape[cursor] = 1
      moveRight()
      currentInstruction = 'C'
    },
    1: () => {
      tape[cursor] = 0
      moveRight()
      currentInstruction = 'F'
    }
  },
  'C':{
    0: () => {
      tape[cursor] = 1
      moveLeft()
      currentInstruction = 'C'
    },
    1: () => {
      tape[cursor] = 1
      moveLeft()
      currentInstruction = 'A'
    }
  },
  'D':{
    0: () => {
      tape[cursor] = 0
      moveLeft()
      currentInstruction = 'E'
    },
    1: () => {
      tape[cursor] = 1
      moveRight()
      currentInstruction = 'A'
    }
  },
  'E':{
    0: () => {
      tape[cursor] = 1
      moveLeft()
      currentInstruction = 'A'
    },
    1: () => {
      tape[cursor] = 0
      moveRight()
      currentInstruction = 'B'
    }
  },
  'F':{
    0: () => {
      tape[cursor] = 0
      moveRight()
      currentInstruction = 'C'
    },
    1: () => {
      tape[cursor] = 0
      moveRight()
      currentInstruction = 'E'
    }
  },
}

while (iterations<iterationsToComplete){
  if(iterations%1000000 == 0){
    console.log(`${iterations} iterations done. Tape length: ${tape.length}. `)
  }
  iterations++
  instructions[currentInstruction][tape[cursor]]()
}

console.log(tape.filter(item => item == 1).length)
