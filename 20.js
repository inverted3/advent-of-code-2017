let input = require('fs').readFileSync('20-input.txt',{encoding: 'utf8'}).split('\n')

const _ = require('lodash')

class Particle {
  constructor(id, location, velocity, accelration){
    this.id = id
    this.location = location
    this.velocity = velocity
    this.accelration = accelration
    this.destroyed = false
  }

  tick(){
    for (let axis in this.location){
      this.velocity[axis] += this.accelration[axis]
      this.location[axis] += this.velocity[axis]
    }
  }

  destroy(){
    this.destroyed = true
  }

  getDistance(){
    return Math.abs(this.location[0])+Math.abs(this.location[1])+Math.abs(this.location[2])
  }

  getAccelerations(){
    return Math.abs(this.accelration[0])+Math.abs(this.accelration[1])+Math.abs(this.accelration[2])
  }
}

let particles = []

for(lineNumber in input){
  let parts = input[lineNumber].match(/([0-9|\-,]{3,})/g)
  particles.push(new Particle(
    lineNumber,
    parts[0].split(',').map(item => parseInt(item)),
    parts[1].split(',').map(item => parseInt(item)),
    parts[2].split(',').map(item => parseInt(item))
  ))
  particles.sort((a,b) => {
    return a.getAccelerations()<b.getAccelerations() ? -1 : 1
  })
}

while(true){
  for (particle of particles){
    particle.tick()
  }
  let liveParticles = particles.filter(particle => !particle.destroyed)
  let toDelete = []
  for (particle of liveParticles){
    for(checkedParticle of liveParticles){
      if (particle.id != checkedParticle.id && _.isEqual(particle.location, checkedParticle.location)){
        toDelete.push(particle)
        toDelete.push(checkedParticle)
      }
    }
  }
  for(particle of toDelete){
    particle.destroy()
  }
  let particlesLeft = particles.filter(particle => !particle.destroyed).length
  console.log(`${particlesLeft} particles left`) // Star 2
}