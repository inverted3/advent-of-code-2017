const transpose = function(array){
  let result = array.slice().map(line => line.slice())
  result = result[0].map((col, i) => result.map(row => row[i]));
  return result
}

const variations = {
  flipH: function(array){
    let result = array.slice().map(line => line.slice())
    for (let line in result){
      result[line] = result[line].slice().reverse()
    }
    return result
  },
  
  flipV: function(array){
    let result = array.slice().map(line => line.slice())  
    return result.reverse()
  },
  
  rotate90: function(array){
    let result = array.slice().map(line => line.slice())  
    return transpose(result.reverse())
  },
  
  rotate180: function(array){
    let result = array.slice().map(line => line.slice())  
    return rotate90(rotate90(result))  
  },
  
  rotate270: function(array){
    let result = array.slice().map(line => line.slice())  
    return rotate(rotate90(rotate90(result)))
  }
}

let input = require('fs').readFileSync('21-input.txt',{encoding: 'utf8'}).split('\n')

let image =
`.#.
..#
###`.split('\n').map(line => line.split(''))

const iterations = 5

for (let i=0; i<iterations; i++){
  if (image.length%2 == 0){

  } else {

  }
}

const divideParts =(image,partSize) => {
  let parts = []
  let imageSize = image.length
  for (let i=0; i<(imageSize/partSize); i++){
    let part = []
    for (let col=0; col<partSize; col++){
      for (let row=0; row<partSize; row++){
        part[col][row] = 
      }    
    }

  }
}

const joinParts =(parts) => {
  
}
      
