const fs = require('fs')
const _ = require('lodash')

let inputFileStream = fs.createReadStream('4-input.txt')
let lineReader = require('readline').createInterface(inputFileStream)

let passPhrases = []

let isAnagram = function(phraseA,phraseB){
  phraseA = phraseA.split('')
  phraseB = phraseB.split('')
  if(phraseA.sort().join('') == phraseB.sort().join('')) return true
  return false
}

let hasAnagrams = function(phrase){
  for (let i=0; i<phrase.length; i++){
    for (let j=0; j<phrase.length; j++){
      if (i!=j && isAnagram(phrase[i],phrase[j])) {
  
        return true      
      }
    }
  }
}

lineReader.on('line', function(line){
  passPhrases.push(line.split(' '))
})

lineReader.on('close', function(){
  console.log('Passphrases count: ',passPhrases.length)
  let invalidPassphrases = 0
  for (let passPhrase of passPhrases){
    if (_.uniq(passPhrase).length != passPhrase.length){
      invalidPassphrases++
    }
  }
  console.log(passPhrases.length-invalidPassphrases, ' (Star 1)')
  let invalidPassphrasesWithAnagrams = 0
  for (let passPhrase of passPhrases){
    if (hasAnagrams(passPhrase)){
      invalidPassphrasesWithAnagrams++
    }
  }
  console.log(passPhrases.length-invalidPassphrasesWithAnagrams, ' (Star 2)')
  
})
