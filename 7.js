let lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('7-input.txt')
})

let programs = {}
let tree = {}

/*

tree
  program
  weight
  childrenNames
  children
    program
    weight
    childrenNames
    ..
    program
    weight
    childrenNames
    children
    ..

*/

lineReader.on('line', function (line) {
  let extraction = line.match(/^([a-z]*) \(([0-9]*)\)(?: (?:->) ((?:[a-z]*,? ?)*))?/)
  let program = {
    name: extraction[1],
    weight: parseInt(extraction[2]),
    children: {},
    childrenNames: []
  }
  if (extraction[3]){
    program.childrenNames = extraction[3].split(', ')
  }
  programs[program.name]=program
})

let attachChildren = function(node){
  if (node.childrenNames.length){
    for (let child of node.childrenNames){
      node.children[child] = programs[child]
      for (let program in node.children){
        attachChildren(node.children[program])
      }
    }
  }
}

let getWeight = function(node){
  if (node.childrenNames.length){
    let totalWeight = node.weight
    for (let child in node.children){
      totalWeight += getWeight(node.children[child])
    }
    return totalWeight
  } else {
    return node.weight
  }
}

let allEqual = function(arr){
  if (!arr.length) return true
  let first = arr[0]
  for (let i=1; i<arr.length; i++){
    if (arr[i]!=first) return false
  } 
  return true
}

let findImbalanced = function(node){
  let weightsInNode = []
  for (let childName in node.children){
    weightsInNode.push(getWeight(node.children[childName]))
  }
  if(!allEqual(weightsInNode)){
    console.log('found imbalance')
    for (let childName in node.children){
      console.log(childName, node.children[childName], getWeight(node.children[childName]))
      findImbalanced(node.children[childName])
    }
  } else {
    for (let childName in node.children){
      findImbalanced(node.children[childName])
    }
  }
}

lineReader.on('close', function(){
  const root = 'uownj'
  // const root = 'tknk'
  tree[root] = programs[root]
  attachChildren(tree[root])
  // console.log(JSON.stringify(tree))
  for (let child in tree[root].children){
    // console.log(tree[root].children[child], getWeight(tree[root].children[child]))
  }
  findImbalanced(tree[root])
})

