let input = '165,1,255,31,87,52,24,113,0,91,148,254,158,2,73,153'

module.exports.knotHash = function(input){
  let list = []
  const listLength = 256
  
  for (let i=0; i<listLength; i++){
    list.push(i)
  }
  
  let processedInput = input.split('').map((char)=> char.charCodeAt(0))
  processedInput = processedInput.concat([17, 31, 73, 47, 23])
  
  let reverseSubset = function(arr,start,length){
    let reverseArr = []
    let tempArr = arr.slice()
    for (let i=start; i<start+length; i++){
      reverseArr.push(tempArr[i%arr.length])
    }
    reverseArr = reverseArr.reverse()
    for (let i=start; i<start+length; i++){
      tempArr[i%arr.length] = reverseArr.shift()
    }
    return tempArr
  }
  
  let skipSize = 0
  let pointer = 0
  
  for (let j = 0; j<64; j++){
    for (let i=0; i<processedInput.length; i++){
      list = reverseSubset(list, pointer, processedInput[i])
      pointer += processedInput[i]
      pointer += skipSize
      pointer %= list.length
      skipSize++
    }
  }
  
  let denseHash = []
  
  for (let block=0; block<16; block++){
    let xorSum = 0
    for (let char=0; char<16; char++){
      xorSum ^= list[char+block*16]
    }
    denseHash.push(xorSum)
  }
  
  denseHash = denseHash.map((item) => {
    let result = (item).toString(16)
    if (result.length==1) result = '0'+result
    return result
  })
  denseHash = denseHash.join('')
  return denseHash 
}

// console.log('hash is: ', module.exports.knotHash(input))