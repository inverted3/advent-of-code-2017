let input = require('fs').readFileSync('24-input.txt',{encoding: 'utf8'}).split('\n')

input = input.map(part => part.split('/').map(item =>parseInt(item)))

let componentStrength = (component) => {
  return component[0]+component[1]
}

let maxBridgeStrength = (index, openPort, components, bridge) => {
  let strength = componentStrength(components[index])
  
  let leftoverComponents = components.slice()
  leftoverComponents.splice(index,1)
  
  let connectables = []

  for (component in leftoverComponents){
    if  (leftoverComponents[component][0] == openPort){
      connectables.push([component,1])
    } 
    if (leftoverComponents[component][1] == openPort){
      connectables.push([component,0])
    } 
  }

  if(!connectables.length) {
    return strength
  }

  let strengths = []
  for (component of connectables){
    strengths.push(
      maxBridgeStrength(parseInt(component[0]),leftoverComponents[component[0]][component[1]], leftoverComponents)
    )
  }

  let maxStrength = strengths.reduce((acc,item) => {
    if (item>acc) return item
    return acc
  },0) 

  return strength + maxStrength

}


// Trying all the valid entry points:
console.log(JSON.stringify(maxBridgeStrength(2, 43, input, [])))
console.log(JSON.stringify(maxBridgeStrength(27, 5, input, [])))
console.log(JSON.stringify(maxBridgeStrength(34, 19, input, [])))
console.log(JSON.stringify(maxBridgeStrength(44, 16, input, [])))