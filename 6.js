const _ = require('lodash')

let banks = [0,5,10,0,11,14,13,4,11,8,8,7,1,4,12,11]
// banks = [0,2,7,0]
let snapshots = []

let bankToRestribute = function(banks){
  let max = 0
  let selectedBank
  for (let i=0; i<banks.length; i++){
    if (banks[i]>max) {
      max = banks[i]
      selectedBank = i
    }
  }
  return selectedBank
}

let nextBank = function(bankNumber){
  return (bankNumber+1)%banks.length
}

let sawOne = false
let sawTwo = false
let loopingSnapshot
let cycles = 0
let cycleInLoop = 0
while(!sawTwo){
  cycles++
  if (sawOne) cycleInLoop++
  snapshots.push(banks.slice())
  let selectedBank = bankToRestribute(banks)
  let blocksToDistribute = banks[selectedBank]
  banks[selectedBank] = 0
  let pointer = selectedBank
  while(blocksToDistribute>0){
    pointer = nextBank(pointer)
    banks[pointer]+= 1
    blocksToDistribute--
  }
  for(let snapshot of snapshots){
    if (sawOne && _.isEqual(banks, loopingSnapshot)){
      sawTwo = true
      console.log(cycleInLoop) // Star 2
      process.exit()
    } else if (!sawOne && _.isEqual(banks, snapshot)){
      sawOne = true
      loopingSnapshot = snapshot
      console.log(cycles) // Star 1
      break
    }
  }
}