let generate = function(value, factor, divider){
  return (value * factor) % divider
}

let decToBinary = function(num){
  let result = parseInt(num).toString(2)
  while (result.length<32) result = '0'+result
  return result
}

let lowestSixteen = function(value){
  return decToBinary(value).substr(16)
}

const genAfactor = 16807
const genBfactor = 48271
const globalDivider = 2147483647

let pairs = 40000000
let matches = 0

// Generator A starts with 883
// Generator B starts with 879
let genAvalue = 883
let genBvalue = 879

while(pairs){
  pairs--
  let genAresult = generate(genAvalue, genAfactor, globalDivider)
  let genBresult = generate(genBvalue, genBfactor, globalDivider)
  if (lowestSixteen(genAresult)==lowestSixteen(genBresult)){
    matches++
    console.log(`${pairs} pairs left. ${matches} matches.`)
  }
  genAvalue = genAresult
  genBvalue = genBresult
}

console.log(matches) // star 1

pairs = 5000000
matches = 0
console.log('star 2:')
while(pairs){
  pairs--
  let genAacceptableResult = false 
  while(!genAacceptableResult){
    let genAresult = generate(genAvalue, genAfactor, globalDivider)
    if (genAresult%4==0) genAacceptableResult = genAresult
    genAvalue = genAresult
  }
  let genBacceptableResult = false 
  while(!genBacceptableResult){
    let genBresult = generate(genBvalue, genBfactor, globalDivider)
    if (genBresult%8==0) genBacceptableResult = genBresult
    genBvalue = genBresult
  }
  if (lowestSixteen(genAacceptableResult)==lowestSixteen(genBacceptableResult)){
    matches++
    console.log(`${pairs} pairs left. ${matches} matches.`)
  }
}
console.log(matches) // star 2
