let input = require('fs').readFileSync('19-input.txt',{encoding: 'utf8'}).split('\n')

for (line in input){
  input[line] = input[line].split('')
}

let cursor = {
  x: input[0].indexOf('|'),
  y: 0
}

let direction = {
  x: 0,
  y: 1
}

let letters = '' // For star 1

let charAtCursor = input[cursor.y][cursor.x]
let stepCount = 0 // For star 2

while(charAtCursor != ' '){
  stepCount++
  if(charAtCursor.match(/[A-Z]/)){
    letters += charAtCursor
  }
  cursor.x += direction.x
  cursor.y += direction.y
  charAtCursor = input[cursor.y][cursor.x]
  if(charAtCursor == '+'){
    if(direction.y){
      if (
        cursor.x-1 >=0
        && input[cursor.y][cursor.x-1] != ' '
      ){
        direction = {
          x: -1,
          y: 0
        }
      }
      if (
        cursor.x+1 < input[0].length
        && input[cursor.y][cursor.x+1] != ' '
      ){
        direction = {
          x: 1,
          y: 0
        }
      } 
    } else if(direction.x){
      if (
        cursor.y-1 >=0
        && input[cursor.y-1][cursor.x] != ' '
      ){
        direction = {
          x: 0,
          y: -1
        }
      }
      if (
        cursor.y+1 < input.length
        && input[cursor.y+1][cursor.x] != ' '
      ){
        direction = {
          x: 0,
          y: 1
        }
      } 
    }
  }
}
console.log(letters, stepCount)