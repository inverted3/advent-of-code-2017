let input = require('fs').readFileSync('22-input.txt',{encoding: 'utf8'}).split('\n').map(line => line.split(''))

let cursor = {
  row: 12,
  col: 12,
  direction: 'U'
}

let turnLeft = () => {
  switch (cursor.direction){
    case 'U':
      cursor.direction = 'L'
      break
    case 'R':
      cursor.direction = 'U'
      break
    case 'D':
      cursor.direction = 'R'
      break
    case 'L':
      cursor.direction = 'D'
      break
  }
}

let turnRight = () => {
  switch (cursor.direction){
    case 'U':
      cursor.direction = 'R'
      break
    case 'R':
      cursor.direction = 'D'
      break
    case 'D':
      cursor.direction = 'L'
      break
    case 'L':
      cursor.direction = 'U'
      break
  }
}

let step = () => {
  switch (cursor.direction){
    case 'U':
      if(cursor.row==0){
        input.unshift(Array(input[0].length).fill('.'))
        cursor.row++
      }
      cursor.row--
      break
    case 'R':
      if(cursor.col==(input[0].length-1)){
        input.map(row => row.push('.'))
      }
      cursor.col++
      break
    case 'D':
      if(cursor.row==(input.length-1)){
        input.push(Array(input[0].length).fill('.'))
      }
      cursor.row++
      break
    case 'L':
      if(cursor.col==0){
        input.map(row => row.unshift('.'))
        cursor.col++
      }    
      cursor.col--
      break
  }
}

let drawMap = () => {
  console.log(`Size: ${input.length} Rows, ${input[0].length} Columns. Cursor: ${cursor.col}, ${cursor.row} heading ${cursor.direction}`)
  for (let row in input){
    let output = ''
    for (let col in input[row]){
      if (cursor.col == col && cursor.row == row){
        output += `[${input[row][col]}]`
      } else {
        output += ` ${input[row][col]} `
      }
    }
    console.log(output)
  }
}

let bursts = 10000000
let infectionCount = 0
while(bursts>0){
  // drawMap()
  bursts--
  switch(input[cursor.row][cursor.col]){
    case '.':
      turnLeft()
      break
    case 'W':
      break
    case '#':
      turnRight()
      break
    case 'F':
      turnLeft()
      turnLeft()
      break
  }
  switch(input[cursor.row][cursor.col]){
    case '.':
      input[cursor.row][cursor.col]='W'
      break
    case 'W':
      input[cursor.row][cursor.col]='#'
      infectionCount++
      break
    case '#':
      input[cursor.row][cursor.col]='F'
      break
    case 'F':
      input[cursor.row][cursor.col]='.'
      break
  }
  step()
}
console.log(infectionCount)
