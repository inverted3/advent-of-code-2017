let buffer = [0]
let skipSize = 382
let cursor = 0
let totalSteps = 2017
let currentStep = 0

let nextStep = function(){
  return (cursor+skipSize)%buffer.length
}

while(currentStep<totalSteps){
  currentStep++
  cursor = nextStep()
  buffer.splice(cursor+1,0,currentStep)
  cursor++
}

console.log(buffer[buffer.indexOf(2017)+1]) // Star 1

skipSize = 382
totalSteps = 50000000
cursor = 0
let output = 0

for (let i=1; i<=totalSteps; i++){
  cursor += 1 + skipSize
  cursor %= i
  if (cursor == 0) {
    output = i
  }
}

console.log(output)