let input = `set b 65
set c b
jnz a 2
jnz 1 5
mul b 100
sub b -100000
set c b
sub c -17000
set f 1
set d 2
set e 2
set g d
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8
sub d -1
set g d
sub g b
jnz g -13
jnz f 2
sub h -1
set g b
sub g c
jnz g 2
jnz 1 3
sub b -17
jnz 1 -23`.split('\n')

const part2 = x => {
  let nonprimes = 0;
  for (let n = x; n <= x + 17000; n += 17) {
      let d = 2;
      while (n % d !== 0) d++;
      if (n !== d) nonprimes++;
  }

  return nonprimes;
};

console.log(part2(65*100+100000))

let registers = {
  a: 1
}
let pointer = 0

registers[b] = 65
registers[c] = registers[b]
if (registers[a]!=0){
  registers[b] *= 100
  registers[b] += 100000
  registers[c] = registers[b] + 17000
}
registers[f] = 1
registers[d] = 1
registers[d] = 1
 
const getValue = function(value){
  let result
  if (isNaN(parseInt(value))){
    result = registers[value]
  } else {
    result = parseInt(value)
  }
  return result
}

let mulCounter = 0
while(pointer>=0 && pointer < input.length){
  // console.log('executing line ', pointer, input[pointer].split(' '))
  let command = input[pointer].split(' ')
  let instruction = command[0]
  let firstArgument = command[1]
  if (!registers.hasOwnProperty(firstArgument)){
    registers[firstArgument] = 0
  }
  let secondArgument = getValue(command[2])
  if(isNaN(secondArgument)) {
    // console.warn('not a number')
  }
  // console.log(command, instruction, firstArgument, secondArgument)
  
  pointer++
  
  switch(instruction){
    case 'set':
      registers[firstArgument] = secondArgument
      break
    case 'sub':
      registers[firstArgument] -= secondArgument
      break
    case 'mul':
      registers[firstArgument] *= secondArgument
      mulCounter++
      break
    case 'jnz':
      if (getValue(firstArgument) != 0){
        pointer += secondArgument - 1  // because the pointer was already advanced by one
      }
      break
  }
  console.log(JSON.stringify(registers), pointer)
}

console.log('done', registers)


