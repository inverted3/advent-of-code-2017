const knotHash = require('./10.js').knotHash

let input = 'vbqugkhl'
let diskSize = 128

let disk = []
let visitMap = []

let hexToBinary = function(num){
  let result = parseInt(num, 16).toString(2)
  while (result.length<4) result = '0'+result
  return result
}

let used = 0

for (let row = 0; row<diskSize; row++){
  disk.push([])
  visitMap.push([])
  let rowBits = knotHash(input+'-'+row).split('')
  rowBits = rowBits.map(hex => hexToBinary(hex))
  rowBits = rowBits.join('').split('')
  for (let bit=0; bit<diskSize; bit++){
    disk[row].push(rowBits[bit]=='1') 
    visitMap[row].push(false)
  }
  used += rowBits.filter(bit => bit=='1').length
}

let markRegion = function(coords){
  visitMap[coords.x][coords.y] = true
  //left
  if (coords.x-1>=0 && !visitMap[coords.x-1][coords.y] && disk[coords.x-1][coords.y]){
    markRegion({x: coords.x-1, y: coords.y})
  }
  //right
  if (coords.x+1<diskSize && !visitMap[coords.x+1][coords.y] && disk[coords.x+1][coords.y]){
    markRegion({x: coords.x+1, y: coords.y})
  }
  //top
  if (coords.y-1>=0 && !visitMap[coords.x][coords.y-1] && disk[coords.x][coords.y-1]){
    markRegion({x: coords.x, y: coords.y-1})
  }
  //bottom
  if (coords.y+1<diskSize && !visitMap[coords.x][coords.y+1] && disk[coords.x][coords.y+1]){
    markRegion({x: coords.x, y: coords.y+1})
  }
}

let findNextRegion = function(){
  let counter =0
  while(counter<Math.pow(diskSize,2)){
    if (
      disk[counter%diskSize][Math.floor(counter/diskSize)] == '1'
      && !visitMap[counter%diskSize][Math.floor(counter/diskSize)]
    ){
      return {
        x: counter%diskSize,
        y: Math.floor(counter/diskSize)
      }
    }
    counter++
  }
  return false
}

let regions = 0

while(findNextRegion()){
  markRegion(findNextRegion())
  regions++
}

console.log(used) // Star 1

console.log(regions) // Star 2


