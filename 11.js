let input = require('fs').readFileSync('11-input.txt',{encoding: 'utf8'})

let currentLocation = {
  x: 0,
  y: 0
}

input = input.split(',')

let walk = function(initialLocation, direction){
  switch(direction){
    case 'n':
      return { x: initialLocation.x, y: initialLocation.y-1 }
    case 'ne':
      return { x: initialLocation.x+1, y: initialLocation.y-1 }
    case 'se':
      return { x: initialLocation.x+1, y: initialLocation.y }
    case 's':
      return { x: initialLocation.x, y: initialLocation.y+1 }    
    case 'sw':
      return { x: initialLocation.x-1, y: initialLocation.y+1 }    
    case 'nw':
      return { x: initialLocation.x-1, y: initialLocation.y }
  }
}

let getZ = function(coords){
  return 0-coords.x-coords.y
}

let cubeDistance = function(coords){
  return (Math.abs(coords.x) + Math.abs(coords.y) + Math.abs(getZ(coords))) / 2  
}

let maxDistance = 0

for (let i=0; i<input.length; i++){
  currentLocation = walk(currentLocation, input[i])
  if (cubeDistance(currentLocation)>maxDistance) maxDistance = cubeDistance(currentLocation)
}
console.log(`distance: ${cubeDistance(currentLocation)}. Max ever: ${maxDistance}`)
