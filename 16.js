let state = 'abcdefghijklmnop'
let input = require('fs').readFileSync('16-input.txt',{encoding: 'utf8'}).split(',')

let spinState = function(state, size){
  let result = state.split('')
  let shiftsLeft = size
  while(shiftsLeft){
    shiftsLeft--
    let temp = result[result.length-1]
    for (let i=result.length-1; i>0; i--){
      result[i] = result[i-1]
    }
    result[0]=temp
  }
  return result.join('')
}

let swapPositions = function(state, posA, posB){
  let result = state.split('')
  let temp = result[posA]
  result[posA] = result[posB]
  result[posB] = temp
  return result.join('')
}

let swapPrograms = function(state, progA,progB){
  let result = state.split('')
  let posA = result.indexOf(progA)
  let posB = result.indexOf(progB)
  result = swapPositions(result.join(''), posA, posB)
  return result
}

let seen = {}

let dance = function(){
  if (seen[state]){
    state = seen[state]
  } else {
    let initialState = state
    for (let i=0; i<input.length; i++){
      let command = input[i].substr(0,1)
      let args = input[i].substr(1)
      switch(command){
        case 's':
          state = spinState(state, parseInt(args))
          // console.log(input[i], 'spinning (s)', parseInt(args))
          break
        case 'x':
          state = swapPositions(state, parseInt(args.split('/')[0]),parseInt(args.split('/')[1]))
          // console.log(input[i], 'swapping (x)', parseInt(args.split('/')[0]),parseInt(args.split('/')[1]))
          break
        case 'p':
          state = swapPrograms(state, args.split('/')[0],args.split('/')[1])
          // console.log(input[i], 'swappingbyname (p)', args.split('/')[0],args.split('/')[1])
          break
      }
    }
    seen[initialState] = state
  }
}

dance()

console.log(state) // Star 1

let dances = 1000000000 -1
let lastTime = Date.now()
while (dances>0){
  dance()
  if (dances%10000000==0){
    console.log(`${dances} dances left. ${Object.keys(seen).length} keys in memory. ${Date.now()-lastTime}ms since last milestone`)
    lastTime = Date.now()
  }
  dances--
}

console.log(state) // Star 2