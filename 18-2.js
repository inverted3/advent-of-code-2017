let input = require('fs').readFileSync('18-input.txt',{encoding: 'utf8'}).split('\n')

let programs = {
  0: {
    id: 0,
    queue: [],
    waiting: false,
    terminated: false,
    pointer: 0,
    registers: {
      p: 0
    }
  },
  1: {
    id: 1,
    queue: [],
    waiting: false,
    terminated: false,
    pointer: 0,
    registers: {
      p: 1
    }
  }
}


let programBvaluesSent = 0

const stepProgram = function(program){

  const getValue = function(value){
    let result
    let parsedInt = parseInt(value)
    if (isNaN(parsedInt)){
      result = program.registers[value]
    } else {
      result = parsedInt
    }
    return result
  }

  if(program.pointer < 0 || program.pointer > input.length-1){
    console.log('Pointer out of bounds: terminating program ', program.id)
    program.terminated = true
  }

  let otherProgram = program.id == 0 ? programs[1] : programs[0]
  let command = input[program.pointer].split(' ')
  let instruction = command[0]
  let firstArgument = command[1]
  if (!program.registers.hasOwnProperty(firstArgument)){
    program.registers[firstArgument] = 0
  }
  let secondArgument = getValue(command[2])

  program.pointer++
    
  switch(instruction){
    case 'snd':
      otherProgram.queue.push(getValue(firstArgument))
      otherProgram.waiting = false
      if(program.id ==1){
        programBvaluesSent++
      }
      break
    case 'set':
      program.registers[firstArgument] = secondArgument
      break
    case 'add':
      program.registers[firstArgument] += secondArgument
      break
    case 'mul':
      program.registers[firstArgument] *= secondArgument
      break
    case 'mod':
      program.registers[firstArgument] %= secondArgument
      break
    case 'rcv':
      if (program.queue.length>0){
        program.registers[firstArgument] = program.queue.shift()
      } else {
        program.waiting = true
        program.pointer-- // undo pointer advance because this step will be re-executed once a value is recieved
      }
      break
    case 'jgz':
      if (getValue(firstArgument) > 0){
        program.pointer += secondArgument - 1  // because the pointer was already advanced by one
      }
      break
  }  
}

while(!(programs[0].terminated && programs[1].terminated)){
  if(
    programs[0].waiting && programs[1].waiting ||
    programs[0].terminated && programs[1].waiting ||
    programs[0].waiting && programs[1].terminated
  ){
    programs[0].terminated = true
    programs[1].terminated = true
  }
  let time = Date.now()

  while(!programs[0].terminated && !programs[0].waiting){
    stepProgram(programs[0])
  }
  while(!programs[1].terminated && !programs[1].waiting){
    stepProgram(programs[1])
  }
}
console.log('DONE!', programBvaluesSent)