let jumps = []
let offsets = []

const fs = require('fs')
const _ = require('lodash')

let inputFileStream = fs.createReadStream('5-input.txt')
let lineReader = require('readline').createInterface(inputFileStream)

lineReader.on('line', function(line){
  jumps.push(parseInt(line))
  offsets.push(0)
})

lineReader.on('close', function(){
  let pointer = 0
  let steps = 0
  // jumps = [0 ,3,  0,  1,  -3]
  
  while(pointer<jumps.length){
    steps++
    let valueAtPointer = jumps[pointer]+offsets[pointer]
    if (valueAtPointer>=3){
      offsets[pointer] -= 1
    } else {
      offsets[pointer] += 1
    }
    pointer += valueAtPointer
  }
  console.log(steps) // Star 2
})

