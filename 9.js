const fs = require('fs')

let input = fs.readFileSync('9-input.txt',{encoding: 'utf8'})
// let input = '{{<{!>}<<<<<<>{{}{}}<!!><<<<><!!>}}'

input = input.split('')
let initialLength = input.length
let pointer = 0
let exclamationPoints = 0
while(pointer<input.length){
  if (input[pointer]=='!'){
    input.splice(pointer+1,1)
    exclamationPoints+=2
  }
  pointer++
}

input = input.join('')
// console.log(input)
let garbagePieces = input.match(/(<.*?>)/g).length
input = input.replace(/(<.*?>)/g,'')
let garbageAmount = initialLength - (input.length + (garbagePieces*2) + exclamationPoints)

input = input.replace(/,/g,'').split('')

console.log('garbage amount:', garbageAmount) //Star 2

let currentLevel =0
let score = 0
for (let i=0; i<input.length; i++){
  if(input[i]=='{'){
    currentLevel++

  } else {
    score += currentLevel
    currentLevel--
  }
}
console.log('total score:',score)
// console.log(input.join(''))
